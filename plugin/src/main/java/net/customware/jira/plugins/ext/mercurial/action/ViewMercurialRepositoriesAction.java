package net.customware.jira.plugins.ext.mercurial.action;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;

import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Manage 1 or more repositories
 */
public class ViewMercurialRepositoriesAction extends MercurialActionSupport
{

    public ViewMercurialRepositoriesAction()
    {
    }

    public Collection<MercurialManager> getActiveRepositories()
    {
        return getRepositories(true);
    }

    public Collection<MercurialManager> getInactiveRepositories()
    {
        return getRepositories(false);
    }

    private Collection<MercurialManager> getRepositories(boolean active)
    {
        Collection<MercurialManager> ms;
        if (active) {
            ms = getMultipleRepoManager().getActiveRepositoryList();
        } else {
            ms = getMultipleRepoManager().getInactiveRepositoryList();
        }
        List<MercurialManager> mercurialManagers = new ArrayList<MercurialManager>(ms);

        Collections.sort(
                mercurialManagers,
                new Comparator<MercurialManager>()
                {
                    public int compare(MercurialManager left, MercurialManager right)
                    {
                        // Make the inactive ones appear first
                        String l = StringUtils.defaultString(left.getDisplayName());
                        String r = StringUtils.defaultString(right.getDisplayName());
                        return l.compareTo(r);
                    }
                }
        );

        return mercurialManagers;
    }

    public Collection<MercurialManager> getSubrepositories()
    {
        return getMultipleRepoManager().getSubrepositoriesList();
    }
}
